FROM maven:latest

ARG BUILDNAME

WORKDIR /var/www/html

CMD pwd
CMD ls -a
COPY ${BUILDNAME} /var/www/html
#COPY cicd/httpd.conf /usr/local/apache2/conf/httpd.conf
#COPY cicd/impact_vhost_httpd.conf /usr/local/apache2/conf/impact_vhost_httpd.conf
#COPY cicd/.htaccess /var/www/html/impact/
COPY cicd/docker-entrypoint.sh /usr/local/bin/

RUN unzip /var/www/html/${BUILDNAME} && \
#rm /var/www/html/${BUILDNAME} && \
pwd && \
ls -ltr /var/www/html/

ENV DOCROOT="/var/www/html" \
  SERVER_NAME="impact.docker-app-dev.lfg.com"

RUN chmod +x /usr/local/bin/docker-entrypoint.sh

ENTRYPOINT ["docker-entrypoint.sh"]
#CMD ["httpd-foreground"]
